﻿using IzendaCommon.IzendaBoundary;
using IzendaCommon.Model;
using IzendaCommon.SampleUserDB;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MVCCoreDM1.CustomClaims;
using MVCCoreDM1.ViewModels;
using System.Threading.Tasks;

namespace MVCCoreDM1.Areas.Main.Controllers
{
    [Area("Main")]
    [ClaimRequirement("IsAdmin", "True")]
    public class TenantController : Controller
    {
        #region Variables
        private readonly string _defaultConnectionString;
        #endregion

        #region CTOR
        public TenantController(IConfiguration configuration) => _defaultConnectionString = configuration.GetConnectionString("DefaultConnection");
        #endregion

        #region Methods
        // GET: Main/Tenant
        public IActionResult Index()
        {
            var tenantList = SampleUserDbHelper.GetAllTenants(_defaultConnectionString);

            return View(tenantList);
        }

        // GET: Main/Tenant/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var tenant = await SampleUserDbHelper.GetTenantById(id, _defaultConnectionString);

            if (tenant == null)
                return NotFound();

            var izendaTenant = await GetIzendaTenantDetail(tenant);

            var tenantVM = new TenantVM()
            {
                Tenant = tenant,
                TenantDetailName = izendaTenant.Name
            };

            return View(tenantVM);
        }

        private static async Task<TenantDetail> GetIzendaTenantDetail(Tenant tenant)
        {
            var izendaAdminToken = IzendaTokenAuthorization.GetIzendaAdminToken();
            var izendaTenant = await SampleUserDbHelper.GetIzendaTenantByTenantId(tenant.Name, izendaAdminToken);

            return izendaTenant;
        }

        // GET: Main/Tenant/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Main/Tenants/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(TenantVM tenantVM)
        {
            if (ModelState.IsValid)
            {
                // check user DB first
                var isTenantExisting = SampleUserDbHelper.GetTenantByName(tenantVM.Tenant.Name, _defaultConnectionString);

                if (isTenantExisting == null)
                {
                    var izendaAdminToken = IzendaTokenAuthorization.GetIzendaAdminToken();
                    
                    // try to create a new tenant at izenda DB
                    var success = await IzendaEndPoint.CreateTenant(tenantVM.TenantDetailName, tenantVM.Tenant.Name, izendaAdminToken);

                    if (success)
                    {
                        await SampleUserDbHelper.SaveTenantAsync(tenantVM.Tenant, _defaultConnectionString);
                    
                        return RedirectToAction(nameof(Index));
                    }
                    ModelState.AddModelError(string.Empty, "Failed to create a new tenant. Tenant already exists in DB.");
                }
                else
                    ModelState.AddModelError(string.Empty, "Failed to create a new tenant. Tenant already exists in DB.");
            }
            return View(tenantVM);
        }
        #endregion
    }
}
