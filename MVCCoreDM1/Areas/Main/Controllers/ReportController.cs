﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Web;

namespace MVCCoreDM1.Areas.Main.Controllers
{
    [Area("Main")]
    public class ReportController : Controller
    {
        #region Variables
        private readonly ILogger<ReportController> _logger;
        #endregion

        #region CTOR
        public ReportController(ILogger<ReportController> logger)
        {
            _logger = logger;
        }
        #endregion

        #region Methods
        public IActionResult Index()
        {
            // not supposed to be routed Index view
            return NotFound();
        }

        /// <summary>
        /// Render izenda Report Designer view
        /// </summary>
        /// <returns>View</returns>
        public IActionResult ReportDesigner()
        {
            return View();
        }

        /// <summary>
        /// Render Izenda Report List page
        /// </summary>
        /// <returns>Reports View</returns>
        public IActionResult Reports()
        {
            return View();
        }

        /// <summary>
        /// Render Report Viewer
        /// use case example: sub report (new window option) / from starter kit menu (Report Viewer)
        /// </summary>
        /// <param name="id">Report id used for rendering</param>
        /// <returns>ReportViewer View</returns>
        [Route("izenda/report/view/{id}")]
        public IActionResult ReportViewer(string id)
        {
            var query = Request.Query;
            dynamic filters = new ExpandoObject();
            foreach (string key in query.Keys)
            {
                ((IDictionary<string, object>)filters).Add(key, query[key]);
            }

            ViewBag.overridingFilterQueries = JsonConvert.SerializeObject(filters);
            ViewBag.Id = id;
            return View();
        }

        /// <summary>
        /// Action for rendering Izenda Report Part
        /// Use case example: export pdf
        /// </summary>
        /// <param name="id">report part id</param>
        /// <param name="token">token from url</param>
        /// <returns>ReportPart View</returns>
        [Route("izenda/viewer/reportpart/{id}")]
        public IActionResult ReportPart(Guid id, string token)
        {
            ViewBag.Id = id;
            ViewBag.Token = string.IsNullOrWhiteSpace(token) ? HttpUtility.UrlEncode(Request.Cookies["access_token"]) : token;

            return View();
        }

        /// <summary>
        /// Action for rendering page with multiple report parts.
        /// </summary>
        /// <returns>ReportParts View</returns>
        public IActionResult ReportParts()
        {
            return View();
        }
        #endregion
    }
}
