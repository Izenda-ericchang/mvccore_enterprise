﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MVCCoreDM1.ViewModels;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Diagnostics;

namespace MVCCoreDM1.Areas.Landing.Controllers
{
    [Area("Main")]
    public class HomeController : Controller
    {
        #region Variables
        private readonly ILogger<HomeController> _logger;
        #endregion

        #region CTOR
        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }
        #endregion

        #region Methods
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Render izenda BI view
        /// </summary>
        /// <returns>View</returns>
        public IActionResult Izenda()
        {
            return View();
        }

        /// <summary>
        /// Render izenda Settings view
        /// </summary>
        /// <returns>View</returns>
        [Route("/Settings")] // route from izenda BI page (Explore)
        [Route("Main/Home/Settings")] // route from nav-bar
        public IActionResult Settings()
        {
            return View();
        }

        /// <summary>
        /// this is a duplicate but required to route to report designer from Izenda Explore page.
        /// </summary>
        /// <returns></returns>
        [Route("/ReportDesigner")] // route from izenda BI page (Explore)
        public IActionResult ReportDesigner()
        {
            return View();
        }

        [Route("izenda/myprofile")]
        public IActionResult ExportManager()
        {
            return View();
        }

        /// <summary>
        /// Action for rendering Izenda I-Frame from exported URL
        /// </summary>
        /// <param name="id">report id</param>
        /// <returns>IframeViewer View</returns>
        [Route("izenda/report/iframe/{id}")]
        public IActionResult IframeViewer(string id)
        {
            var query = Request.Query;
            dynamic filters = new System.Dynamic.ExpandoObject();
            foreach (string key in query.Keys)
            {
                ((IDictionary<string, object>)filters).Add(key, query[key]);
            }

            ViewBag.overridingFilterQueries = JsonConvert.SerializeObject(filters);
            ViewBag.Id = id;
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        #endregion
    }
}
