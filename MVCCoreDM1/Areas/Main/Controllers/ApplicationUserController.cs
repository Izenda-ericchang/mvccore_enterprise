﻿using IzendaCommon.IzendaBoundary;
using IzendaCommon.Model;
using IzendaCommon.SampleUserDB;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using MVCCoreDM1.CustomClaims;
using MVCCoreDM1.ViewModels;
using System.Linq;
using System.Threading.Tasks;

namespace MVCCoreDM1.Areas.Main.Controllers
{
    [Area("Main")]
    [ClaimRequirement("IsAdmin", "True")]
    public class ApplicationUserController : Controller
    {
        #region Variables
        private readonly string _defaultConnectionString;
        #endregion

        #region CTOR
        public ApplicationUserController(IConfiguration configuration) => _defaultConnectionString = configuration.GetConnectionString("DefaultConnection");
        #endregion

        #region Methods
        // GET: Main/ApplicationUser
        public async Task<IActionResult> Index()
        {
            var applicationUserList = await SampleUserDbHelper.GetAllUsers(_defaultConnectionString);

            return View(applicationUserList);
        }

        // GET: Main/ApplicationUser/Create
        public IActionResult Create()
        {
            var applicationUserVM = new ApplicationUserVM()
            {
                TenantList = SampleUserDbHelper.GetAllTenants(_defaultConnectionString).Select(i => new SelectListItem
                {
                    Text = i.Name,
                    Value = i.Id.ToString()
                })
            };

            return View(applicationUserVM);
        }

        // POST: Main/ApplicationUser/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ApplicationUserVM applicationUserVM)
        {
            if (ModelState.IsValid)
            {
                if (applicationUserVM.SelectedTenantId != null)
                {
                    int? tenantId = applicationUserVM.SelectedTenantId;
                    applicationUserVM.IsAdmin = false;
                }

                var userRole = !string.IsNullOrEmpty(applicationUserVM.SelectedRole) ? applicationUserVM.SelectedRole : "Employee";

                var user = new ApplicationUser
                {
                    UserName = applicationUserVM.UserID,
                    Email = applicationUserVM.UserID,
                    Role = userRole,
                    TenantId = applicationUserVM.SelectedTenantId
                };

                var isDuplicate = await SampleUserDbHelper.IsUserExistingInUserDB(user.UserName, user.TenantId, _defaultConnectionString);

                if (isDuplicate)
                {
                    ModelState.AddModelError(string.Empty, "Failed to create a new user. User already exists in user DB.");
                }
                else
                {
                    await SampleUserDbHelper.SaveUserAsync(user.UserName, user.Email, user.Role, user.TenantId, applicationUserVM.Password, _defaultConnectionString);

                    var izendaAdminAuthToken = IzendaTokenAuthorization.GetIzendaAdminToken();
                    var tenant = await SampleUserDbHelper.GetTenantById(user.TenantId, _defaultConnectionString);

                    // Create a new user at Izenda DB
                    var success = await IzendaEndPoint.CreateIzendaUser(
                        tenant?.Name,
                        applicationUserVM.UserID,
                        applicationUserVM.LastName,
                        applicationUserVM.FirstName,
                        applicationUserVM.IsAdmin,
                        userRole,
                        izendaAdminAuthToken);

                    if (success)
                        return LocalRedirect("/Main/ApplicationUser");

                    ModelState.AddModelError(string.Empty, "Failed to create a new user. User already exists in DB.");
                }
            }

            return View(new ApplicationUserVM() {
                TenantList = SampleUserDbHelper.GetAllTenants(_defaultConnectionString).Select(i => new SelectListItem
                {
                    Text = i.Name,
                    Value = i.Id.ToString()
                })
            });
        }

        // GET: Dynamic retrieval of roles based on selected tenant
        [HttpGet("GetAllRoles")]
        public async Task<IActionResult> GetAllRoles(string selectedTenant)
        {
            var adminToken = IzendaTokenAuthorization.GetIzendaAdminToken();

            if (selectedTenant == "Select Tenant") // default selection means no tenant (null - system level)
                selectedTenant = null; // system level

            var izendaTenant = await SampleUserDbHelper.GetIzendaTenantByTenantId(selectedTenant, adminToken);
            var roleDetailsByTenant = await IzendaEndPoint.GetAllIzendaRoleByTenant(izendaTenant?.Id ?? null, adminToken);

            var roles = roleDetailsByTenant.Select(r => new { r.Id, r.Name }).ToList();
            SelectList _roleList = new SelectList(roles, "Id", "Name");

            return new JsonResult(_roleList);
        }
        #endregion
    }
}
