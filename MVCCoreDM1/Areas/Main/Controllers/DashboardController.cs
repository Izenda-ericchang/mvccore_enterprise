﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Dynamic;

namespace MVCCoreDM1.Areas.Main.Controllers
{
    [Area("Main")]
    public class DashboardController : Controller
    {
        #region Variables
        private readonly ILogger<DashboardController> _logger;
        #endregion

        #region CTOR
        public DashboardController(ILogger<DashboardController> logger)
        {
            _logger = logger;
        }
        #endregion

        #region Methods
        public IActionResult Index()
        {
            // not supposed to be routed Index view
            return NotFound();
        }
        
        /// <summary>
        /// Action for rendering Izenda Dashboard Designer page
        /// </summary>
        /// <returns>DashboardDesigner View</returns>
        [Route("/dashboard/new")] // route from izenda BI page (Explore)
        [Route("Main/Dashboard/DashboardDesigner")] // route from nav-bar
        public IActionResult DashboardDesigner()
        {
            return View();
        }

        /// <summary>
        /// Action for rendering Izenda Dashboard List page
        /// </summary>
        /// <returns>Dashboards View</returns>
        public IActionResult Dashboards()
        {
            return View();
        }

        /// <summary>
        /// Action for rendering a dashboard viewer.
        /// </summary>
        /// <param name="id">The dashboard ID</param>
        /// <returns>DashboardViewer View</returns>
        [Route("izenda/dashboard/edit/{id}")]
        public IActionResult DashboardViewer(string id)
        {
            var query = Request.Query;
            dynamic filters = new ExpandoObject();
            foreach (string key in query.Keys)
            {
                ((IDictionary<string, object>)filters).Add(key, query[key]);
            }

            ViewBag.Id = id;
            ViewBag.filters = JsonConvert.SerializeObject(filters);
            return View();
        }
        #endregion
    }
}
