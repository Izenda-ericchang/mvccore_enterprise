﻿using IzendaCommon.IzendaBoundary;
using IzendaCommon.SampleUserDB;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MVCCoreDM1.ViewModels;
using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace MVCCoreDM1.Areas.Main.Controllers
{
    [Area("Main")]
    public class SampleAuthController : Controller
    {
        #region variables
        private string _defaultConnectionString;
        #endregion

        #region CTOR
        public SampleAuthController(IConfiguration configuration)
        {
            _defaultConnectionString = configuration.GetConnectionString("DefaultConnection");
        }
        #endregion

        #region Methods
        public IActionResult Index() => NotFound(); // not supposed to be routed Index view

        [HttpGet]
        [Route("/Account/AccessDenied")]
        public IActionResult AccessDenied()
        {
            return View();
        }

        // GET: Main/IzendaSampleAuth/Login
        public IActionResult Login()
        {
            return View();
        }

        // GET: Main/IzendaSampleAuth/Logout
        public async Task<IActionResult> Logout(string returnUrl = null)
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            return LocalRedirect(returnUrl);
        }

        // POST: Main/Tenants/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginUserVM loginUserVM)
        {
            var returnUrl = Url.Content("~/");
            if (ModelState.IsValid)
            {
                IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
                bool.TryParse(configuration.GetValue<string>("AppSettings:Settings:useADlogin"), out bool useADlogin);

                // Active Directory login: In this case, all users in the Active Directory should be mapped into user db (SampleUsers)
                if (useADlogin && !string.IsNullOrEmpty(loginUserVM.Tenant)) // if tenant is null, then assume that it is system level login. Go to the ValidateLogin which is used for regular login process first
                {
                    bool success = ValidateActiveDirectoryLogin(loginUserVM.Tenant);

                    if (success)
                    {
                        //proceed the next step
                        if (await AuthenticateUser(loginUserVM.Email, loginUserVM.Password, loginUserVM.Tenant))
                        {
                            return LocalRedirect(returnUrl);
                        }
                    }
                }
                else // standard login
                {
                    var loginAttempt = await AuthenticateUser(loginUserVM.Email, loginUserVM.Password, loginUserVM.Tenant);

                    if (loginAttempt)
                        return LocalRedirect(returnUrl);
                }
            }

            // If we got this far, something failed, redisplay form
            ModelState.AddModelError(string.Empty, "Failed to login. Please check your credential.");
            return View(loginUserVM);
        }

        private async Task<bool> AuthenticateUser(string userName, string password, string tenantName)
        {
            var users = SampleUserDbHelper.GetUserList(userName, _defaultConnectionString);

            // Can't find user. Login failed.
            if (!users.Any())
                return false;

            // find specific user by tenant
            var currentUser = users.FirstOrDefault(u => u.TenantUniqueName == tenantName);

            // no matching user + tenant found
            if (currentUser == null)
                return false;

            // check if password matches
            var userPasswordFromDB = string.Empty;

            try
            {
                userPasswordFromDB = IzendaTokenAuthorization.GetPassword(currentUser.Password);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

                // failed to verify hashed password
                return false;
            }

            if (password != null && password.Equals(userPasswordFromDB))
            {
                // retrieve role list assigned to user
                var roleList = SampleUserDbHelper.GetUserRoles(userName, _defaultConnectionString).Result;

                var claims = new List<Claim>();

                var nameClaim = new Claim("userName", userName);
                claims.Add(nameClaim);

                if (tenantName != null)
                {
                    var tenantClaim = new Claim("tenantName", tenantName);
                    claims.Add(tenantClaim);
                }

                if (roleList.Any(r => r == "Admin"))
                {
                    var roleClaim = new Claim("IsAdmin", "True");
                    claims.Add(roleClaim);
                }
                else
                {
                    var roleClaim = new Claim("IsAdmin", "False");
                    claims.Add(roleClaim);
                }

                var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

                var authProperties = new AuthenticationProperties
                {
                    #region Properties can be set for Cookie based authentication
                    //AllowRefresh = <bool>,
                    // Refreshing the authentication session should be allowed.

                    //ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(10),
                    // The time at which the authentication ticket expires. A 
                    // value set here overrides the ExpireTimeSpan option of 
                    // CookieAuthenticationOptions set with AddCookie.

                    //IsPersistent = true,
                    // Whether the authentication session is persisted across 
                    // multiple requests. When used with cookies, controls
                    // whether the cookie's lifetime is absolute (matching the
                    // lifetime of the authentication ticket) or session-based.

                    //IssuedUtc = <DateTimeOffset>,
                    // The time at which the authentication ticket was issued.

                    //RedirectUri = <string>
                    // The full path or absolute URI to be used as an http 
                    // redirect response value.
                    #endregion
                };

                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme,
                    new ClaimsPrincipal(claimsIdentity),
                    authProperties);

                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Login with Active Directory information.
        /// Please refer to the following link to get more information on Active Directory 
        /// https://docs.microsoft.com/en-us/windows-server/identity/ad-ds/get-started/virtual-dc/active-directory-domain-services-overview
        /// </summary>
        private bool ValidateActiveDirectoryLogin(string tenant)
        {
            var userName = Environment.UserName;
            var userDomainName = Environment.UserDomainName;
            var authenticationType = ContextType.Domain;

            if (!string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(userDomainName))
            {
                using (var context = new PrincipalContext(authenticationType, Environment.UserDomainName))
                {
                    var userPrincipal = UserPrincipal.FindByIdentity(context, IdentityType.SamAccountName, userName);

                    if (userPrincipal != null)
                    {
                        var email = userPrincipal.EmailAddress;
                        var users = SampleUserDbHelper.GetUserList(email, _defaultConnectionString); // get user list from DB

                        // if matches with tenant information, then authentication is successfull.
                        return users != null ? users.FirstOrDefault(u => u.TenantUniqueName == tenant) != null : false;
                    }
                }
            }

            return false;
        }
        #endregion
    }
}
