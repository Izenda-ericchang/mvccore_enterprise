﻿using IzendaCommon.IzendaBoundary;
using IzendaCommon.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace MVCCoreDM1.Controllers.ApiController
{
    [Authorize]
    public class UserController : Controller
    {
        #region Methods
        /// <summary>
        /// Generate a token containing encrypted UserInfo for an Izenda user.
        /// </summary>
        /// <returns>Access token for a user</returns>
        [HttpGet]
        [Route("user/GenerateToken")]
        public JsonResult GenerateToken()
        {
            var identity = (ClaimsIdentity)User.Identity;
            var username = identity.FindFirst("userName");
            var tenantName = identity.FindFirst("tenantName");

            var user = new UserInfo { UserName = username?.Value, TenantUniqueName = tenantName?.Value };
            var token = IzendaTokenAuthorization.GetToken(user);
            return Json(new { token });
        }
        #endregion
    }
}
