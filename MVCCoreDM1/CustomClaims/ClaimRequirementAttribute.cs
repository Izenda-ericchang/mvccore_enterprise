﻿using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace MVCCoreDM1.CustomClaims
{
    public class ClaimRequirementAttribute : TypeFilterAttribute
    {
        #region CTOR
        public ClaimRequirementAttribute(string claimType, string claimValue) : base(typeof(ClaimRequirementFilter))
        {
            Arguments = new object[] { new Claim(claimType, claimValue) };
        } 
        #endregion
    }
}
