﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;
using System.Security.Claims;

namespace MVCCoreDM1.CustomClaims
{
    public class ClaimRequirementFilter : IAuthorizationFilter
    {
        #region Variables
        private readonly Claim _claim;
        #endregion

        #region CTOR
        public ClaimRequirementFilter(Claim claim) => _claim = claim;
        #endregion

        #region Methods
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var hasClaim = context.HttpContext.User.Claims.Any(c => c.Type == _claim.Type && c.Value == _claim.Value);
            if (!hasClaim)
                context.Result = new ForbidResult();
        } 
        #endregion
    }
}
