﻿using System.ComponentModel.DataAnnotations;

namespace MVCCoreDM1.ViewModels
{
    public class TenantVM
    {
        #region Properties
        public IzendaCommon.Model.Tenant Tenant { get; set; }

        [Display(Name = "Tenant Name")]
        [Required]
        public string TenantDetailName { get; set; } 
        #endregion
    }
}
