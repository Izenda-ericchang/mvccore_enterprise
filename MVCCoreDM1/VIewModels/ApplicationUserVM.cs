﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MVCCoreDM1.ViewModels
{
    public class ApplicationUserVM
    {
        #region Properties
        [Display(Name = "Tenant*")]
        public int? SelectedTenantId { get; set; }

        [Display(Name = "Role")]
        public string SelectedRole { get; set; }

        [Display(Name = "Is Admin")]
        public bool IsAdmin { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "User ID")]
        public string UserID { get; set; }

        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; } 

        [Required]
        public string Password { get; set; }

        public IEnumerable<SelectListItem> TenantList { get; set; }

        public IEnumerable<SelectListItem> RoleList { get; set; }
        #endregion
    }
}
