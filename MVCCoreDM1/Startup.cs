using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace MVCCoreDM1
{
    public class Startup
    {
        #region Properties
        public IConfiguration Configuration { get; }
        #endregion

        #region CTOR
        public Startup(IConfiguration configuration) => Configuration = configuration;
        #endregion

        #region Methods
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Cookie Authentication implemented.
            // For more information, please refer to following link
            // https://docs.microsoft.com/en-us/aspnet/core/security/authentication/cookie?view=aspnetcore-5.0
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options => 
                {
                    // add options if required. e.g., options.ExpireTimeSpan
                });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("CanCreateUserAndTenant", policy => policy.RequireClaim("IsAdmin", "True"));
            });

            services.AddControllersWithViews().AddRazorRuntimeCompilation();
            services.AddRazorPages();

            services.ConfigureApplicationCookie(options =>
            {
                options.LoginPath = $"/Main/SampleAuth/Login";
                options.LogoutPath = $"/Main/SampleAuth/Logout";
                options.AccessDeniedPath = $"/Main/SampleAuth/Login";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            var cookiePolicyOptions = new CookiePolicyOptions
            {
                MinimumSameSitePolicy = SameSiteMode.Unspecified
            };
            app.UseCookiePolicy(cookiePolicyOptions);

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{area=Main}/{controller=Home}/{action=Index}/{id?}");

                endpoints.MapRazorPages();

                endpoints.MapControllerRoute(
                    name: "IframeViewer",
                    pattern: "{area=Main}/{controller=Home}/{action=IframeViewer}");

                // for export process required html image (e.g., pdf, gauge, ...)
                endpoints.MapControllerRoute(
                    name: "ReportPart",
                    pattern: "{area=Main}/{controller=Report}/{action=ReportPart}");

                // for sub report - new window and access from report viewer menu at nav bar
                endpoints.MapControllerRoute(
                    name: "ReportViewer",
                    pattern: "{area=Main}/{controller=Report}/{action=ReportViewer}");

                // for access from dashboard viewer menu at nav bar
                endpoints.MapControllerRoute(
                    name: "DashboardViewer",
                    pattern: "{area=Main}/{controller=Dashboard}/{action=DashboardViewer}");

                // Reroute
                endpoints.MapControllerRoute(
                    name: "izenda_reportviewer",
                    pattern: "{area=Main}/{controller=Fallback}/{action=Izenda}");

                // Reroute
                endpoints.MapControllerRoute(
                    name: "izenda_report",
                    pattern: "{area=Main}/{controller=Fallback}/{action=Izenda}");

                // Reroute
                endpoints.MapControllerRoute(
                    name: "izenda_reportviewerpopup",
                    pattern: "{area=Main}/{controller=Fallback}/{action=Izenda}");

                // Reroute
                endpoints.MapControllerRoute(
                    name: "izenda_dashboard",
                    pattern: "{area=Main}/{controller=Fallback}/{action=Izenda}");

                // Reroute
                endpoints.MapControllerRoute(
                    name: "izenda_settings",
                    pattern: "{area=Main}/{controller=Fallback}/{action=Izenda}");

                // Reroute
                endpoints.MapControllerRoute(
                    name: "izenda_new",
                    pattern: "{area=Main}/{controller=Fallback}/{action=Izenda}");
            });
        }
        #endregion
    }
}
