﻿namespace IzendaCommon.Settings
{
    public static class AuthSettings
    {
        #region Static Variables
        public static string IzendaAPIUrl = "http://localhost:5001/api/";
        public static string RSAPrivateKey = "MIICXAIBAAKBgQCP3jU5YrfnJ22hvi3qURFvD9s+m2AbO5oeq+D1YM7eU7GWOiA7A5L8Lr2VE5J4Ec6noL+CymFQD/xROuZnP4YQZOnI1VtCsDYIcZF9/m+8bg0xU6of3ZIYIKth8F3W7DpfOPNfdDF1E+m2vVXc7+nIO5zRWwm/IReIiWdgHzeMkwIDAQABAoGAFHdk30ozEEq/IWR9LVl6tJqEXZExUwkfwTTsC+xJhuj6w/zL4KlUVn94ZTCYxG+qvupdeYfnNRe5FNM0u25sLfT58QnAEDsTmPOfAXwXLU0KASUaJWmkZVzXVmyDJgNpq3RhzVRLIMzXZ6lDo+aPgse8QIi7QduEfH4a7UHkRx0CQQDU7dSGlPXGOO9feCi89IqI4Bd2K4Ei6Wa9M+r30QHZNDrSw59kBv1vw93nSXEmzVoxzCxfQUEbWwvG5XDePFLnAkEArPgrUx1mNpIM3lmcRRnvdBgA19ECQO5+BDB7Wso+FUCT1Vqy24nOIbW4YVBx5Ofyg6ENMFzcmtE2sUCKmcDvdQJAYeyu3OYM0gjFLabZNW1RePl2UMZZMXEd6s3Btg7NRtMaamvFFf+Q9qGhoAjKnQm7/ULk9TnRc38/v00tx0b+6QJBAJj8Iu2vahFIGkOrhHEF4GmRNWWtAk5F4oV4EBKcIE8Rv++L+ff9KaldHchMq3/nuvK7RnwJFkGEW9homm4lXGUCQBohFb1n3l0vsDmfXWwyat0k/Xla+StsIx/MihFqxcU8JHltJXzlIcNi2n7XGArMdFzm+8sCADcjHG8b/TJdq9I=";
        #endregion
    }
}
