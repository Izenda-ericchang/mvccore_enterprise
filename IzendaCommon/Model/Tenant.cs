﻿using System.ComponentModel.DataAnnotations;

namespace IzendaCommon.Model
{
    public class Tenant
    {
        #region Properties
        public int Id { get; set; }

        [Display(Name = "Tenant ID")]
        public string Name { get; set; }
        #endregion

        #region CTOR
        public Tenant()
        { }

        public Tenant(int id, string name)
        {
            Id = id;
            Name = name;
        }
        #endregion
    }
}
