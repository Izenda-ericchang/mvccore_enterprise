﻿using IzendaCommon.IzendaBoundary;
using IzendaCommon.Model;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace IzendaCommon.SampleUserDB
{
    public static class SampleUserDbHelper
    {
        #region User
        /// <summary>
        /// Get all users from user db
        /// </summary>
        public static async Task<IEnumerable<UserInfo>> GetAllUsers(string connectionString)
        {
            var users = new List<UserInfo>();

            using (var connection = new SqlConnection(connectionString))
            {
                var queryString = $"SELECT UserName, PasswordHash, TenantId FROM SampleUsers;";

                var command = new SqlCommand(queryString, connection);
                command.Connection.Open();
                var reader = await command.ExecuteReaderAsync();

                // Get potential list of users
                while (reader.Read())
                {
                    int? tenantId = null;

                    var id = reader["TenantId"].ToString();
                    if (!string.IsNullOrEmpty(id))
                    {
                        tenantId = int.Parse(id);
                    }

                    var user = new UserInfo
                    {
                        UserName = reader["UserName"].ToString(),
                        Password = reader["PasswordHash"].ToString()
                    };

                    if (tenantId == null) // if tenantId is null, it is system level
                        user.TenantUniqueName = null;
                    else // otherwise tenant level
                        user.TenantUniqueName = GetAllTenants(connectionString).FirstOrDefault(t => t.Id == tenantId)?.Name ?? null;

                    users.Add(user);
                }
                reader.Close();
            }

            return users;
        }

        /// <summary>
        /// Get UserInfo list by username
        /// The same user name can exist in multiple tenants so returns a list (UserInfo)
        /// </summary>
        public static IEnumerable<UserInfo> GetUserList(string username, string defaultConnection)
        {
            var users = new List<UserInfo>();

            using (var connection = new SqlConnection(defaultConnection))
            {
                var queryString = $"SELECT UserName, PasswordHash, TenantId FROM SampleUsers WHERE UserName = '{username}';";

                var command = new SqlCommand(queryString, connection);
                command.Connection.Open();
                var reader = command.ExecuteReader();

                // Get potential list of users
                while (reader.Read())
                {
                    int? tenantId = null;

                    var id = reader["TenantId"].ToString();
                    if (!string.IsNullOrEmpty(id))
                    {
                        tenantId = int.Parse(id);
                    }

                    var user = new UserInfo
                    {
                        UserName = reader["UserName"].ToString(),
                        Password = reader["PasswordHash"].ToString()
                    };

                    if (tenantId == null) // if tenantId is null, it is system level
                        user.TenantUniqueName = null;
                    else // otherwise tenant level
                        user.TenantUniqueName = GetAllTenants(defaultConnection)?.FirstOrDefault(t => t.Id == tenantId)?.Name ?? null;

                    users.Add(user);
                }
                reader.Close();
            }
            return users;
        }

        /// <summary>
        /// Get user id from SampleUsers table with user name
        /// </summary>
        private static async Task<string> GetUserId(string username, string defaultConnectionString)
        {
            var userId = string.Empty;
            using (var connection = new SqlConnection(defaultConnectionString))
            {
                var queryString = $"SELECT Id FROM SampleUsers WHERE UserName = '{username}';";

                var command = new SqlCommand(queryString, connection);
                await command.Connection.OpenAsync();
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    userId = reader["Id"].ToString();
                }
                reader.Close();
            }
            return userId;
        }

        public static async Task<bool> IsUserExistingInUserDB(string username, int? userTenantId, string defaultConnection)
        {
            using (var connection = new SqlConnection(defaultConnection))
            {
                var queryString = $"SELECT UserName, TenantId FROM SampleUsers WHERE UserName = '{username}';";

                var command = new SqlCommand(queryString, connection);
                await command.Connection.OpenAsync();
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    int? tenantId = null;

                    var id = reader["TenantId"].ToString();
                    if (!string.IsNullOrEmpty(id))
                    {
                        tenantId = int.Parse(id);
                    }

                    var name = reader["UserName"].ToString();

                    if (tenantId == null && name == username) // there is a match in system level
                        return true;
                    else if (tenantId == userTenantId && name == username) // match found in tenant level
                        return true;
                    else
                        return false;
                }
                reader.Close();
            }
            return false;
        }

        /// <summary>
        /// SAVE user into user DB
        /// </summary>
        public static async Task SaveUserAsync(string userName, string email, string roleName, int? tenantId, string password, string connectionString)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                var hashedPW = IzendaTokenAuthorization.SetPassword(password);

                await connection.OpenAsync();
                var queryString = tenantId != null ? $"INSERT INTO SampleUsers (Id, UserName, TenantId, PasswordHash)" +
                    $"VALUES(@param0, @param1, @param2, @param3)"
                        : $"INSERT INTO SampleUsers (Id, UserName)" +
                        $"VALUES(@param0, @param1, @param3)";

                using (SqlCommand cmd = new SqlCommand(queryString, connection))
                {
                    cmd.Parameters.Add("@param0", SqlDbType.UniqueIdentifier).Value = Guid.NewGuid();
                    cmd.Parameters.Add("@param1", SqlDbType.NVarChar, 256).Value = userName;
                    cmd.Parameters.Add("@param3", SqlDbType.NVarChar, 256).Value = hashedPW;

                    if (tenantId != null)
                        cmd.Parameters.Add("@param2", SqlDbType.Int, int.MaxValue).Value = tenantId;

                    cmd.ExecuteNonQuery();
                }
            }

            await AssignRoleToUser(userName, roleName, connectionString);
        }
        #endregion

        #region Tenant
        /// <summary>
        /// Get all tenants from user db
        /// </summary>
        public static List<Tenant> GetAllTenants(string connectionString)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                var _tenantNameList = new List<Tenant>();
                var queryString = $"SELECT * FROM SampleTenants;";

                var command = new SqlCommand(queryString, connection);
                command.Connection.Open();
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    var tenant = new Tenant(int.Parse(reader["Id"]?.ToString() ?? string.Empty), reader["Name"].ToString() ?? string.Empty);

                    _tenantNameList.Add(tenant);
                }
                reader.Close();

                return _tenantNameList;
            }
        }

        /// <summary>
        /// Get Tenant by Tenant name (tenant Id - string)
        /// </summary>
        public static Tenant GetTenantByName(string name, string connectionString)
        {
            return GetAllTenants(connectionString).FirstOrDefault(t => t.Name == name);
        }

        /// <summary>
        /// Get Tenant by Tenant Id (nullable int)
        /// </summary>
        public static async Task<Tenant> GetTenantById(int? id, string connectionString)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                var queryString = $"SELECT * FROM SampleTenants;";

                var command = new SqlCommand(queryString, connection);
                await command.Connection.OpenAsync();
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    if (id != null && int.Parse(reader["Id"]?.ToString()) == id)
                    {
                        var tenant = new Tenant(int.Parse(reader["Id"]?.ToString() ?? string.Empty), reader["Name"].ToString() ?? string.Empty);

                        return tenant;
                    }
                }
                reader.Close();

                return null;
            }
        }

        /// <summary>
        /// Retrieve TenantDetail by TenantId
        /// </summary>
        public static async Task<TenantDetail> GetIzendaTenantByTenantId(string tenantId, string authToken)
        {
            var tenants = await WebAPIService.Instance.GetAsync<IList<TenantDetail>>("/tenant/allTenants", authToken);
            if (tenants != null)
                return tenants.FirstOrDefault(x => x.TenantId.Equals(tenantId, StringComparison.InvariantCultureIgnoreCase));

            return null;
        }

        /// <summary>
        /// SAVE tenant into client DB
        /// </summary>
        public static async Task SaveTenantAsync(Tenant tenant, string connectionString)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                await connection.OpenAsync();
                var queryString = $"INSERT INTO SampleTenants (Name) VALUES(@param1)";

                using (SqlCommand cmd = new SqlCommand(queryString, connection))
                {
                    cmd.Parameters.Add("@param1", SqlDbType.NVarChar, 1000).Value = tenant.Name;
                    cmd.ExecuteNonQuery();
                }
            }
        }

        #endregion

        #region Role
        /// <summary>
        /// Get a matched role from the list of Izenda Roles under the selected tenant
        /// </summary>
        private static async Task<RoleDetail> GetIzendaRoleByTenantAndName(Guid? tenantId, string roleName, string authToken)
        {
            var roles = await IzendaBoundary.IzendaEndPoint.GetAllIzendaRoleByTenant(tenantId, authToken);

            if (roles.Any())
                return roles.FirstOrDefault(r => r.Name.Equals(roleName, StringComparison.InvariantCultureIgnoreCase));

            return null;
        }

        /// <summary>
        /// Get all roles as a list - role name string
        /// </summary>
        /// <param name="defaultConnectionString"></param>
        /// <returns></returns>
        public static async Task<IList<string>> GetAllRoles(string defaultConnectionString)
        {
            var roleNameList = new List<string>();

            using (var connection = new SqlConnection(defaultConnectionString))
            {
                var queryString = $"SELECT Name FROM SampleRoles;";

                var command = new SqlCommand(queryString, connection);
                await command.Connection.OpenAsync();
                var reader = await command.ExecuteReaderAsync();

                while (reader.Read())
                {
                    var roleName = reader["Name"].ToString();

                    roleNameList.Add(roleName);
                }
            }

            return roleNameList;
        }

        /// <summary>
        /// Assign a role to user in user db
        /// </summary>
        public static async Task AssignRoleToUser(string userName, string roleName, string connectionString)
        {
            var userId = await GetUserId(userName, connectionString);
            var roleIdToBeAssigned = await GetRoleIdByRoleName(roleName, connectionString);

            using (var connection = new SqlConnection(connectionString))
            {
                await connection.OpenAsync();

                var queryString = $"INSERT INTO SampleUserRoles (UserId, RoleId)" +
                    $"VALUES(@param0, @param1)";

                using (SqlCommand cmd = new SqlCommand(queryString, connection))
                {
                    cmd.Parameters.Add("@param0", SqlDbType.UniqueIdentifier).Value = Guid.Parse(userId);
                    cmd.Parameters.Add("@param1", SqlDbType.UniqueIdentifier).Value = Guid.Parse(roleIdToBeAssigned);

                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Get Role Id by role name in user db
        /// </summary>
        private static async Task<string> GetRoleIdByRoleName(string roleName, string connectionString)
        {
            var roleId = string.Empty;

            using (var connection = new SqlConnection(connectionString))
            {
                var queryString = $"SELECT Id FROM SampleRoles WHERE Name = '{roleName}';";

                var command = new SqlCommand(queryString, connection);
                await command.Connection.OpenAsync();
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    roleId = reader["Id"].ToString();
                }

                return roleId;
            }
        }


        /// <summary>
        /// Retrieve role id list from user id
        /// </summary>
        private static IEnumerable<string> GetRoleIdList(string userId, string defaultConnectionString)
        {
            var roleIdList = new List<string>();
            using (var connection = new SqlConnection(defaultConnectionString))
            {
                var queryString = $"SELECT RoleId FROM SampleUserRoles WHERE UserId = '{userId}';";

                var command = new SqlCommand(queryString, connection);
                command.Connection.Open();
                var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    var roleId = reader["RoleId"].ToString();
                    roleIdList.Add(roleId);
                }
            }
            return roleIdList;
        }

        /// <summary>
        /// Get User Role (string) list
        /// </summary>
        public static async Task<IEnumerable<string>> GetUserRoles(string username, string defaultConnection)
        {
            var userId = await GetUserId(username, defaultConnection); // find out user id from username
            var roleIdList = GetRoleIdList(userId, defaultConnection); // get all role ids assinged to user id
            var roleList = GetRoleList(roleIdList, defaultConnection); // get all roles (string name)

            return roleList;
        }

        /// <summary>
        /// Retrieve role name list from role id
        /// </summary>
        private static IEnumerable<string> GetRoleList(IEnumerable<string> roleIdList, string defaultConnectionString)
        {
            var roleList = new List<string>();
            foreach (var roleId in roleIdList)
            {
                using (var connection = new SqlConnection(defaultConnectionString))
                {
                    var queryString = $"SELECT Name FROM SampleRoles WHERE Id = '{roleId}';";

                    var command = new SqlCommand(queryString, connection);
                    command.Connection.Open();
                    var reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        var roleName = reader["Name"].ToString();

                        roleList.Add(roleName);
                    }
                }
            }
            return roleList;
        }

        /// <summary>
        /// Create a new role at selected tenant
        /// </summary>
        public static async Task<RoleDetail> CreateRole(string roleName, TenantDetail izendaTenant, string authToken)
        {
            var role = await GetIzendaRoleByTenantAndName(izendaTenant != null ? (Guid?)izendaTenant.Id : null, roleName, authToken);

            if (role == null)
            {
                role = new RoleDetail
                {
                    Active = true,
                    Deleted = false,
                    NotAllowSharing = false,
                    Name = roleName,
                    TenantId = izendaTenant != null ? (Guid?)izendaTenant.Id : null
                };

                var response = await WebAPIService.Instance.PostReturnValueAsync<AddRoleResponeMessage, RoleDetail>("role", role, authToken);
                role = response.Role;
            }

            return role;
        }
        #endregion
    }
}
