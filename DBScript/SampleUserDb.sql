USE [SampleUserDb]
GO
/****** Object:  Database [SampleUserDb]    Script Date: 3/5/2021 11:58:03 AM ******/
CREATE DATABASE [SampleUserDb]
GO
ALTER DATABASE [SampleUserDb] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SampleUserDb].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SampleUserDb] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [SampleUserDb] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [SampleUserDb] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [SampleUserDb] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [SampleUserDb] SET ARITHABORT OFF 
GO
ALTER DATABASE [SampleUserDb] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [SampleUserDb] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [SampleUserDb] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [SampleUserDb] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [SampleUserDb] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [SampleUserDb] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [SampleUserDb] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [SampleUserDb] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [SampleUserDb] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [SampleUserDb] SET  ENABLE_BROKER 
GO
ALTER DATABASE [SampleUserDb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [SampleUserDb] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [SampleUserDb] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [SampleUserDb] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [SampleUserDb] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [SampleUserDb] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [SampleUserDb] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [SampleUserDb] SET RECOVERY FULL 
GO
ALTER DATABASE [SampleUserDb] SET  MULTI_USER 
GO
ALTER DATABASE [SampleUserDb] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [SampleUserDb] SET DB_CHAINING OFF 
GO
ALTER DATABASE [SampleUserDb] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [SampleUserDb] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [SampleUserDb] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'SampleUserDb', N'ON'
GO
ALTER DATABASE [SampleUserDb] SET QUERY_STORE = OFF
GO
USE [SampleUserDb]
GO
/****** Object:  Table [dbo].[SampleRoles]    Script Date: 3/5/2021 11:58:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SampleRoles](
	[Id] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](256) NULL,
 CONSTRAINT [PK_AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SampleTenants]    Script Date: 3/5/2021 11:58:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SampleTenants](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Tenants] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SampleUserRoles]    Script Date: 3/5/2021 11:58:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SampleUserRoles](
	[UserId] [nvarchar](450) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SampleUsers]    Script Date: 3/5/2021 11:58:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SampleUsers](
	[Id] [nvarchar](450) NOT NULL,
	[UserName] [nvarchar](256) NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[TenantId] [int] NULL,
 CONSTRAINT [PK_AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[SampleRoles] ([Id], [Name]) VALUES (N'017402b4-ae46-43ca-a6c2-8a996680179b', N'VP')
INSERT [dbo].[SampleRoles] ([Id], [Name]) VALUES (N'ce0ab481-07d4-46ac-b6a4-c6ccf5f2792a', N'Employee')
INSERT [dbo].[SampleRoles] ([Id], [Name]) VALUES (N'f49f3238-c6c0-4c71-91e3-71fe71c3d51a', N'Admin')
INSERT [dbo].[SampleRoles] ([Id], [Name]) VALUES (N'ff8ef4cf-f917-4da4-85a4-776385028670', N'Manager')
SET IDENTITY_INSERT [dbo].[SampleTenants] ON 

INSERT [dbo].[SampleTenants] ([Id], [Name]) VALUES (1, N'DELDG')
INSERT [dbo].[SampleTenants] ([Id], [Name]) VALUES (2, N'NATWR')
INSERT [dbo].[SampleTenants] ([Id], [Name]) VALUES (3, N'RETCL')
SET IDENTITY_INSERT [dbo].[SampleTenants] OFF
INSERT [dbo].[SampleUserRoles] ([UserId], [RoleId]) VALUES (N'1709c974-6067-43db-b5ff-63d0d2d7d87b', N'017402b4-ae46-43ca-a6c2-8a996680179b')
INSERT [dbo].[SampleUserRoles] ([UserId], [RoleId]) VALUES (N'95e3336e-4553-44cb-b214-641525a308e9', N'017402b4-ae46-43ca-a6c2-8a996680179b')
INSERT [dbo].[SampleUserRoles] ([UserId], [RoleId]) VALUES (N'eda4c5a3-7135-49a0-8bc5-709f6a929cbc', N'017402b4-ae46-43ca-a6c2-8a996680179b')
INSERT [dbo].[SampleUserRoles] ([UserId], [RoleId]) VALUES (N'4b43d818-3b9a-4b4d-b5a7-d2dac530b28f', N'ce0ab481-07d4-46ac-b6a4-c6ccf5f2792a')
INSERT [dbo].[SampleUserRoles] ([UserId], [RoleId]) VALUES (N'b64513ce-29a5-415a-ba55-4357050d61d9', N'ce0ab481-07d4-46ac-b6a4-c6ccf5f2792a')
INSERT [dbo].[SampleUserRoles] ([UserId], [RoleId]) VALUES (N'c742c74f-5c03-4ae4-871f-70154e8d7de0', N'ce0ab481-07d4-46ac-b6a4-c6ccf5f2792a')
INSERT [dbo].[SampleUserRoles] ([UserId], [RoleId]) VALUES (N'da30e4db-37da-491e-82e7-88ea5d8b2bf0', N'f49f3238-c6c0-4c71-91e3-71fe71c3d51a')
INSERT [dbo].[SampleUserRoles] ([UserId], [RoleId]) VALUES (N'2c253e56-6e26-4c94-9028-967bc6231f85', N'ff8ef4cf-f917-4da4-85a4-776385028670')
INSERT [dbo].[SampleUserRoles] ([UserId], [RoleId]) VALUES (N'bb951be1-ec5f-4da8-a860-cb2ecf522cdd', N'ff8ef4cf-f917-4da4-85a4-776385028670')
INSERT [dbo].[SampleUserRoles] ([UserId], [RoleId]) VALUES (N'c92a4cdc-09c5-4cdd-a8d5-1271f647c99e', N'ff8ef4cf-f917-4da4-85a4-776385028670')
INSERT [dbo].[SampleUsers] ([Id], [UserName], [PasswordHash], [TenantId]) VALUES (N'1709c974-6067-43db-b5ff-63d0d2d7d87b', N'vp@retcl.com', N'kunc1dS+RxSFGZbi5af3gw==
', 3)
INSERT [dbo].[SampleUsers] ([Id], [UserName], [PasswordHash], [TenantId]) VALUES (N'2c253e56-6e26-4c94-9028-967bc6231f85', N'manager@natwr.com', N'kunc1dS+RxSFGZbi5af3gw==
', 2)
INSERT [dbo].[SampleUsers] ([Id], [UserName], [PasswordHash], [TenantId]) VALUES (N'4b43d818-3b9a-4b4d-b5a7-d2dac530b28f', N'employee@retcl.com', N'kunc1dS+RxSFGZbi5af3gw==
', 3)
INSERT [dbo].[SampleUsers] ([Id], [UserName], [PasswordHash], [TenantId]) VALUES (N'95e3336e-4553-44cb-b214-641525a308e9', N'vp@natwr.com', N'kunc1dS+RxSFGZbi5af3gw==
', 2)
INSERT [dbo].[SampleUsers] ([Id], [UserName], [PasswordHash], [TenantId]) VALUES (N'b64513ce-29a5-415a-ba55-4357050d61d9', N'employee@natwr.com', N'kunc1dS+RxSFGZbi5af3gw==
', 2)
INSERT [dbo].[SampleUsers] ([Id], [UserName], [PasswordHash], [TenantId]) VALUES (N'bb951be1-ec5f-4da8-a860-cb2ecf522cdd', N'manager@deldg.com', N'kunc1dS+RxSFGZbi5af3gw==
', 1)
INSERT [dbo].[SampleUsers] ([Id], [UserName], [PasswordHash], [TenantId]) VALUES (N'c742c74f-5c03-4ae4-871f-70154e8d7de0', N'employee@deldg.com', N'kunc1dS+RxSFGZbi5af3gw==
', 1)
INSERT [dbo].[SampleUsers] ([Id], [UserName], [PasswordHash], [TenantId]) VALUES (N'c92a4cdc-09c5-4cdd-a8d5-1271f647c99e', N'manager@retcl.com', N'kunc1dS+RxSFGZbi5af3gw==
', 3)
INSERT [dbo].[SampleUsers] ([Id], [UserName], [PasswordHash], [TenantId]) VALUES (N'da30e4db-37da-491e-82e7-88ea5d8b2bf0', N'IzendaAdmin@system.com', N'kunc1dS+RxSFGZbi5af3gw==
', NULL)
INSERT [dbo].[SampleUsers] ([Id], [UserName], [PasswordHash], [TenantId]) VALUES (N'eda4c5a3-7135-49a0-8bc5-709f6a929cbc', N'vp@deldg.com', N'kunc1dS+RxSFGZbi5af3gw==
', 1)
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetUserRoles_RoleId]    Script Date: 3/5/2021 11:58:03 AM ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUserRoles_RoleId] ON [dbo].[SampleUserRoles]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_AspNetUsers_Tenant_Id]    Script Date: 3/5/2021 11:58:03 AM ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUsers_Tenant_Id] ON [dbo].[SampleUsers]
(
	[TenantId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SampleUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[SampleRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SampleUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[SampleUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[SampleUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SampleUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[SampleUsers]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUsers_Tenants_Tenant_Id] FOREIGN KEY([TenantId])
REFERENCES [dbo].[SampleTenants] ([Id])
GO
ALTER TABLE [dbo].[SampleUsers] CHECK CONSTRAINT [FK_AspNetUsers_Tenants_Tenant_Id]
GO
USE [master]
GO
ALTER DATABASE [SampleUserDb] SET  READ_WRITE 
GO
