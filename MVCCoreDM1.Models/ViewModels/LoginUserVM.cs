﻿using System.ComponentModel.DataAnnotations;

namespace MVCCoreDM1.Models.ViewModels
{
    public class LoginUserVM
    {
        // Tenant is not "[Required]". Null value is allowed
        [DataType(DataType.Text)]
        [Display(Name = "Tenant", Prompt = "System level login does not require tenant field input")]
        public string Tenant { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }
}
